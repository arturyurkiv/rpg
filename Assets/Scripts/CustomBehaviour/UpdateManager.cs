﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateManager : MonoBehaviour
{
    private void Update()
    {
        for(var i = 0; i < CustomBehaviour._allFixedTicks.Count; i++)
        {
            CustomBehaviour._allTicks[i].Tick();
        }
    }

    private void FixedUpdate()
    {
        for (var i = 0; i < CustomBehaviour._allFixedTicks.Count; i++)
        {
            CustomBehaviour._allTicks[i].FixedTick();
        }
    }

    private void LateUpdate()
    {
        for (var i = 0; i < CustomBehaviour._allFixedTicks.Count; i++)
        {
            CustomBehaviour._allTicks[i].LateTick();
        }
    }



}
