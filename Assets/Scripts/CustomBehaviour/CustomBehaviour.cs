﻿using System.Collections.Generic;
using UnityEngine;

public class CustomBehaviour : MonoBehaviour
{
    public static List<CustomBehaviour> _allTicks = new List<CustomBehaviour>();
    public static List<CustomBehaviour> _allFixedTicks = new List<CustomBehaviour>();
    public static List<CustomBehaviour> _allLateTicks = new List<CustomBehaviour>();

    private void OnEnable()
    {
        _allTicks.Add(this);
        _allFixedTicks.Add(this);
        _allLateTicks.Add(this);
    }

    private void OnDisable()
    {
        _allTicks.Remove(this);
        _allFixedTicks.Remove(this);
        _allLateTicks.Remove(this);
    }

    public void Tick() => OnTick();


    public void FixedTick() => OnFixedTick();

    public void LateTick() => OnLateTick();

    public virtual void OnTick() { }
    
    public virtual void OnFixedTick() { }

    public virtual void OnLateTick() { }




}
