﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskSystemController : CustomBehaviour
{

    public static event Action OnPlayerGetTask;

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Player")
        {
            OnPlayerGetTask.Invoke();
        }
        
    }


}
