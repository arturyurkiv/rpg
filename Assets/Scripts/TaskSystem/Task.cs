﻿using UnityEngine;
using System;

[Serializable]
public class Task 
{
    [SerializeField] private string[] _text;

    [SerializeField] private Aim _aim;

    [SerializeField] private int _count;

    [SerializeField] private bool _isActive = false;



}
