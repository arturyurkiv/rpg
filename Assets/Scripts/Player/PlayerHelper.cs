﻿using UnityEngine;

public class PlayerHelper : CustomBehaviour
{

    [Range(0,1)][SerializeField]
     private float _vertical;

    [Range(0, 1)]
    [SerializeField]
    private float _horizontal;

    private Animator _animator;

    [SerializeField] private bool _enableRootMotion;

    [SerializeField] private bool _attack;
    [SerializeField] private string[] _attackAnimName;



    private void Start()
    {

        _animator = GetComponent<Animator>();
    
    }

    public override void OnTick()
    {
        _enableRootMotion = !_animator.GetBool("CanMove");
        _animator.applyRootMotion = _enableRootMotion;

        if (_enableRootMotion)
            return;

        if(_attack)
        {
            string targetAnim;

            int randomAttack = Random.Range(0, _attackAnimName.Length);
            targetAnim = _attackAnimName[randomAttack];

            _vertical = 0;
            _animator.CrossFade(targetAnim, 0.2f);
            _animator.SetBool("CanMove", false);
            _enableRootMotion = true;
            _attack = false;
        }



        _animator.SetFloat("Vertical", _vertical);
        _animator.SetFloat("Horizontal", _horizontal);

    }
}
