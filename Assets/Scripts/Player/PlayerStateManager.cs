﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateManager : Singleton<PlayerStateManager>
{
    [SerializeField] private float _movementSpeed = 3f;
    [SerializeField] private float _jumpForce = 4;
    [SerializeField] private float _rotationSpeed = 1;


    #region Private field
    private float _vertical;
    private float _horizontal;
    private float _mouseX;
    private float _mouseY;
    private bool _canJump = true;
    private bool _canMove = true;

    private Rigidbody _rigidbody;
    private Animator _animator;

    #endregion

    #region Props

    public GameObject GetPlayer
    {
        get => gameObject;     
    }

    public float Vertical
    {
        get 
        {
            if (!Mouse0)
            {
                return _vertical;
            }
            return 0;
        }               
    }

    public float Horizontal
    {
        get
        {
            if (!Mouse0)
            {
                return _horizontal;
            }
            return 0;
        }
    }

    public float MouseX
    {
        get => _mouseX;
    }

    public float MouseY
    {
        get => _mouseY;
    }

    public float MovementSpeed
    {
        get => _movementSpeed;

        set
        {
            _movementSpeed = value;
        }
       
    }

    public float JumpForce
    {
        get => _jumpForce;
        set
        {
            _jumpForce = value;
        }

    }

    public bool Mouse0
    {
        get
        {
            if (Input.GetMouseButtonDown(0)) return true;

            return false;
        }
    }


    public bool CanJump
    {
        get
        {
            if (Input.GetKeyDown(KeyCode.Space) && _canJump) return true;
           
            return false;
        }
        set
        {
            _canJump = value;
        }
    }
   

    public Rigidbody GetRigidbody
    {
        get => _rigidbody;
    }

    public Animator GetAnimator
    {
        get => _animator;
    }


    #endregion

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponentInChildren<Animator>();

        if (_rigidbody == null || _animator == null)
        { 
            Debug.LogError("Animator && Rigidbody null!!!");
        }
    }


    public override void OnFixedTick()
    {
        _vertical = Input.GetAxis("Vertical");
        _horizontal = Input.GetAxis("Horizontal");

        _mouseX += Input.GetAxis("Mouse X") * _rotationSpeed * Time.deltaTime;
        _mouseY -= Input.GetAxis("Mouse Y") * _rotationSpeed * Time.deltaTime;
        _mouseY = Mathf.Clamp(_mouseY, -35, 60);

    }





}
