﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : CustomBehaviour
{

    private PlayerStateManager _state;

    private void Start()
    {
        _state = PlayerStateManager.Instance;
    }

    public override void OnTick ()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        float horizontal = _state.Horizontal; 
        float vertical = _state.Vertical;
        Vector3 playerMovement = new Vector3(horizontal, 0f, vertical) * _state.MovementSpeed* Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }

    private void Jump()
    {
        if(_state.CanJump == true)
        _state.GetRigidbody.AddForce(new Vector3(0, _state.JumpForce * 100f, _state.JumpForce / 4f));

    } 



}