﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : CustomBehaviour
{
    [SerializeField] private float _damage;

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Enemy"))
        {
            Destroy(coll.gameObject);
        }
    }
}
