﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : CustomBehaviour
{
    [SerializeField] private string[] _attackAnimName;

    private PlayerStateManager _state;
    private bool _enableRootMotion;

    private void Start()
    {
        _state = PlayerStateManager.Instance;
    }

    public override void OnTick()
    {
        _enableRootMotion = !_state.GetAnimator.GetBool("CanMove");
        _state.GetAnimator.applyRootMotion = _enableRootMotion;

        if (_enableRootMotion)
            return;

        if (_state.Mouse0 == true)
        {
            string targetAnim;

            int randomAttack = Random.Range(0, _attackAnimName.Length);
            targetAnim = _attackAnimName[randomAttack];
            _state.GetAnimator.CrossFade(targetAnim, 0.4f);

            _state.GetAnimator.SetBool("CanMove", false);

            _enableRootMotion = true;
        }

        _state.GetAnimator.SetFloat("Vertical", _state.Vertical);
        _state.GetAnimator.SetFloat("Horizontal", _state.Horizontal);

        if (_state.CanJump)
        {
            _state.GetAnimator.CrossFade("Jump", 0.6f);

        }

        var d = _state.GetAnimator.GetLayerIndex("");

    }

}
