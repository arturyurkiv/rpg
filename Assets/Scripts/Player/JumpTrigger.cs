﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTrigger : CustomBehaviour
{

    private void OnTriggerEnter(Collider coll)
    {
        PlayerStateManager.Instance.CanJump = true;

    }

    private void OnTriggerExit(Collider coll)
    {
        PlayerStateManager.Instance.CanJump = false;

    }

}
