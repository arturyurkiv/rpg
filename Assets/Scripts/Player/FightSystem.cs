﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightSystem : CustomBehaviour
{

    [SerializeField] private Collider _rightLegCollider;
    [SerializeField] private Collider _swordCollider;

    public void RightLegHit(int value)
    {
        if (value == 1)
        {
            Debug.Log("Hight Kick");

            _rightLegCollider.enabled = true;
        }
        else
        {
            _rightLegCollider.enabled = false;
        }
    }

    public void SwordHit(int value)
    {
        if (value == 1)
        {
            Debug.Log("Sword Kick");

            _swordCollider.enabled = true;
        }
        else
        {
            _swordCollider.enabled = false;
        }
    }




}
