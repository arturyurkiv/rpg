﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : CustomBehaviour
{
    private PlayerStateManager _state;


   private Transform _target;


    private void Start()
    {
        _state = PlayerStateManager.Instance;

        _target = GameObject.Find("CameraHorder").transform;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public override void OnFixedTick()
    {
        CameraControl();
    }

    private void CameraControl()
    {
        transform.LookAt(_target);
        _target.rotation = Quaternion.Euler(_state.MouseY, _state.MouseX, 0);
        _state.GetPlayer.transform.rotation = Quaternion.Euler(0, _state.MouseX, 0);

    }

}