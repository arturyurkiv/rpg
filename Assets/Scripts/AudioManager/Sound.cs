﻿using System;
using UnityEngine;

[Serializable]
public class Sound
{
    public Sounds name;
    public  AudioClip audioClip;

    [Range(0,1)] public float volume;

    public bool loop = false;

    
    public AudioSource AudioSource {get; set;}

}
