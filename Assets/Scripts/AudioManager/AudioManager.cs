﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Linq;

public class AudioManager : Singleton<AudioManager>
{

    public   Sound[] sounds;

    private void GetSounds()
    {
        foreach (var sound in sounds)
        {
            sound.AudioSource = gameObject.AddComponent<AudioSource>();
            sound.AudioSource.clip = sound.audioClip;

            sound.AudioSource.volume = sound.volume;
            sound.AudioSource.loop = sound.loop;
        }
    }

    /// <summary>
    /// Play the specified SoundName.
    /// </summary>
    /// <param name="sound">Sound name.</param>
    public   void PlayAudio(Sounds sound)
    {
        var currentSound = FindSound(sound);
        currentSound.AudioSource.Play();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sound">Sound name.</param>
    /// <param name="loop">loop music.</param>
    public  void PlayAudio(Sounds sound, bool loop)
    {
        var currenSound = FindSound(sound);
        currenSound.AudioSource.Play();
        currenSound.AudioSource.loop = loop;       
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sound">Sound name.</param>
    /// <param name="volume">Music volume.</param>
    public  void PlayAudio(Sounds sound, float volume)
    {           
        var currenSound = FindSound(sound);      

            if (volume >= 0 && volume <= 1)
            {
                currenSound.AudioSource.volume = volume;

                currenSound.AudioSource.Play();
            }
            else
            {
                Debug.LogWarning("Invalid value in" + currenSound + "sound valume");
            }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sound">Sound name.</param>
    /// <param name="loop">loop music.</param>
    /// <param name="volume">Music volume.</param>
    public  void PlayAudio(Sounds sound, bool loop, float volume )
    {
        var currenSound = FindSound(sound);

            if (volume >= 0 && volume <= 1)
            {
                currenSound.AudioSource.volume = volume;
                currenSound.AudioSource.loop = loop;

                currenSound.AudioSource.Play();
            }
            else
            {
                Debug.LogWarning("Invalid value in" + currenSound + "sound valume");
            }

    }

    /// <summary>
    /// Stop all sound
    /// </summary>
    public  void StopAudio()
    {
        foreach (var sound in sounds)
        {
            sound.AudioSource.Stop();
        }

    }

    /// <summary>
    /// Find sound with name "SoundName"
    /// </summary>
    /// <param name="soundToFind">Sound to find.</param>
    /// <returns></returns>
    private  Sound FindSound(Sounds soundToFind)
    {   
        var currentSound = sounds.SingleOrDefault(sound => sound.name == soundToFind);
            
        if (currentSound != null)
        {
            return currentSound;
        }

        Debug.LogWarning("Song" + currentSound + "not found");
        return null;

    }
    
}